import Vue from "vue";
import LaraconOnlineLogo from "./components/laracon-logo";
import Schedule from "./components/Schedule";
import moment from "moment-timezone";

new Vue({
  el: "#app",
  components: {
    LaraconOnlineLogo,
    Schedule
  },

  data: () => ({
    now: null,
    currentTime: null
  }),

  created() {
    this.now = moment().format("MMMM D h:mm:ssa z");
    this.currentTime = moment();

    setInterval(() => {
      this.now = moment().format("MMMM D h:mm:ssa z");
      this.currentTime = moment();
    }, 1000);
  }
});
